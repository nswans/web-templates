Vue.component('info', {
  template: `
    <footer>
      <span>(C) Noah Swanson 2018-2019 | </span>
      <a class="uk-link-reset" href="mailto:noah@kome.ga">noah@kome.ga</a>
      <span> | </span>
      <a class="uk-link-reset" href="https://twitter.com/kkslaughter2070">@kkslaughter2070</a>
    </footer>
  `
})

app = new Vue({
  el: '#app',
  data: {
    message: 'Hell, World!'
  }
})
