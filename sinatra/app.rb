require 'rubygems'
require 'bundler'

Bundler.require

require_rel 'models'
require_rel 'helpers'

class Website < Sinatra::Base
  set :bind, "0.0.0.0"
  enable :sessions
  set :root, File.dirname(__FILE__)
  disable :raise_errors if (ENV["PRODUCTION"] == "YES")
  disable :show_exceptions if (ENV["PRODUCTION"] == "YES")
  set :haml, format: :html5

  register Sinatra::Namespace
  helpers Sinatra::RequiredParams

  configure do
    # Setup here
  end

  get '/' do
    erb :index
  end

  error 404 do
    erb :error
  end
end

require_relative './controllers/base_controller.rb'
require_rel 'controllers'

Website.run! if __FILE__==$0